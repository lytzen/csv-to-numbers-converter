#!/bin/bash

input=$1

if [ -z $1 ]; then
	echo "USAGE: $0 <csv file from Nordea>"
	echo "OUTPUT: file-<sequence> ready to be imported into Numbers."
	exit 1;
fi

declare -i i
i=1
output=$input-$i
while [ -e $output ]; do
	# File already exists
	i=i+1
	output=$input-$i
done

# convert "," to "." and then ";" to "," then remove leading whitespace and then trailing whitespace and last multiple whitespace into a single

sed -e "s/,/./g" -e "s/;/,/g" -e "s/^[[:space:]]\{1,\}//g" -e "s/[[:space:]]\{1,\}$//g" -e "s/[[:space:]]\{2,\}/ /g" > $output


